# Teste Desenvolvedor BackEnd

## Descrição do projeto

Desenvolva uma aplicação web que permita aos usuários fazerem uma lista de tarefas (to-do list). Cada usuário deve ser capaz de se registrar e fazer login na aplicação. Depois de fazer login, o usuário deve poder adicionar, editar e remover tarefas. As tarefas devem ser exibidas em uma lista ordenada por data de criação, com as tarefas mais recentes no topo da lista. Possuindo uma tabela de Logs de ações da aplicação.

## Requisitos

- A aplicação deve ser desenvolvida em Python usando o framework Django.
- Os usuários devem poder se registrar e fazer login na aplicação.
- Os usuários devem poder adicionar, editar e remover tarefas.
- As tarefas devem ser armazenadas em um banco de dados SQLite.
- As tarefas devem ser exibidas em uma lista ordenada por data de criação, com as tarefas mais recentes no topo da lista.
- Possuir uma tabela de logs
- O código deve ser organizado e bem estruturado.

## Instruções

- Desenvolva a aplicação de acordo com os requisitos acima.
- Faça commits frequentes e com mensagens descritivas.
- Quando terminar, crie um merge request para este repositório.
- Descrição completa de o que foi feito, dentro da Merge Request.

## Critérios de avaliação

- A aplicação atende aos requisitos acima.
- O código é organizado e bem estruturado.
- O código segue as melhores práticas do framework Django.

Boa sorte!